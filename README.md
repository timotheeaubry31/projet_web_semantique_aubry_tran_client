
# Mét & Hauts (Client)

Une application de conseils vestimentaires qui utilise les données météo pour donner des recommandations sur les vêtements à porter pour la journée. L'application peut afficher les prévisions météo pour la journée, y compris les températures, la pluie et la neige prévues ou le vent. En fonction de ces informations, l'application peut donner des recommandations sur les vêtements à porter, tels que des vestes, des manteaux, des parapluies ou des bottes.


## Demo

![Demo](/screenshots/demo.gif)



## Lancer en local

Cloner le project

```bash
  git clone https://gitlab.com/timotheeaubry31/projet_web_semantique_aubry_tran_client.git
```

Aller dans le répertoire du project

```bash
  cd projet_web_semantique_aubry_tran_client
```

Installer les modules manquants

```bash
  npm install
```

Lancer l'application

```bash
  npm start
```

Afficher la page

```http
  http://localhost:3000
```


## Fonctionnalités

- Affichage des données météorologiques (Choix de la date)
- Affichage des vêtements suggérés (Choix du sexe)


## Technologies

**Client:** ReactJS, HTML, CSS, JS


## Screenshots

![Screen 1](/screenshots/screen1.PNG)
![Screen 2](/screenshots/screen2.PNG)
![Screen 3](/screenshots/screen3.PNG)
![Screen 4](/screenshots/screen4.PNG)
![Screen 5](/screenshots/screen5.PNG)
![Screen 6](/screenshots/screen6.PNG)

