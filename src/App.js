import { useCallback, useEffect, useState } from 'react';
import './App.css';
import Clothes from './Components/Clothes';
import Datepicker from './Components/Datepicker';
import MaterialUISwitch from './Components/SwitchButton';
import Weather from './Components/Weather';
import moment from 'moment';
import axios from 'axios';
import BigNumber from 'bignumber.js'

function App() {

    const [checked, setChecked] = useState(false);

    const [currentDate, setCurrentDate] = useState(moment().format('YYYY-MM-DD'));

    const [temperature, setTemperature] = useState(0)
    const [precipitation, setPrecipitation] = useState(0)
    const [cloudCover, setCloudCover] = useState(0)
    const [vent, setVent] = useState(0)

    const [max, setMax] = useState(0)
    const [min, setMin] = useState(0)

    const maxDate = '2023-06-23'
    const minDate = '2023-03-08'

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    const fetchDataFromDate = useCallback(async () => {
        const sparqlServerUrl = "http://localhost:3030/sparqluedo/query";
        const config = { headers: { "Content-Type": "application/x-www-form-urlencoded" } };
        const DateQuery = "query=" + encodeURIComponent("PREFIX myont: <http://www.semanticweb.org/adminetu/ontologies/2023/4/untitled-ontology-2#> SELECT DISTINCT * WHERE { ?individu myont:aUneDate ?date . ?individu myont:aUneTemperature ?temperature . ?individu myont:aUnTauxDePrecipitation ?precipitation . ?individu myont:aUneVitesseDeVent ?vent . ?individu myont:aUnTauxEnsoleillement ?ensoleillement . FILTER regex(str(?date), \"^" + currentDate + "\")}");

        try {
            const response1 = await axios.post(sparqlServerUrl, DateQuery, config);

            //console.log(response1); // Traitez les données de réponse ici

            if (response1.status === 200) {
                setTemperature(getTemperature(response1).toFixed(1))
                setCloudCover(getCloudCover(response1))
                setVent(getVent(response1).toFixed(1))
                setMin(getMin(response1).toFixed(1));
                setMax(getMax(response1).toFixed(1));
                setPrecipitation(getPrecipitation(response1))
            }

        } catch (error) {
            console.log(error);
        }
    }, [currentDate])

    const getTemperature = (res) => {
        var somme = 0
        var nbElt = 0
        res.data.results.bindings.forEach((element) => {
            if (element.temperature.value !== "nan") {
                somme += parseFloat(element.temperature.value)
                nbElt += 1
            }
        })

        return somme / nbElt
    }

    const getCloudCover = (res) => {
        var somme = 0
        var nbElt = 0
        res.data.results.bindings.forEach((element) => {
            if (element.ensoleillement.value !== "nan") {
                const newNumber = new BigNumber(parseInt(element.ensoleillement.value))
                somme = newNumber.plus(somme)
                nbElt += 1
            }
        })
        return somme / nbElt
    }

    const getVent = (res) => {
        var somme = 0
        var nbElt = 0
        res.data.results.bindings.forEach((element) => {
            if (element.vent.value !== "nan") {
                somme += parseFloat(element.vent.value)
                nbElt += 1
            }
        })

        return somme / nbElt
    }

    const getMin = (res) => {
        var min = null
        res.data.results.bindings.forEach(element => {
            if (element.temperature.value !== 'nan') {
                if (min === null) {
                    min = parseFloat(element.temperature.value)
                } else {
                    if (min > parseFloat(element.temperature.value)) {
                        min = parseFloat(element.temperature.value)
                    }
                }
            }
        })
        return parseFloat(min)
    }


    const getMax = (res) => {
        var max = null
        res.data.results.bindings.forEach(element => {

            if (element.temperature.value !== 'nan') {
                if (max === null) {
                    max = parseFloat(element.temperature.value)
                } else {
                    if (max < parseFloat(element.temperature.value)) {
                        max = parseFloat(element.temperature.value)
                    }
                }
            }
        })
        return max
    }

    const getPrecipitation = (res) => {
        var precipitation = 0
        res.data.results.bindings.forEach(element => {
            if (element.precipitation.value !== 'nan') {
                if (element.precipitation.value > 0) {
                    precipitation = parseFloat(element.precipitation.value)
                }
            }
        })
        return precipitation
    }

    useEffect(() => {
        fetchDataFromDate()
    }, [currentDate, fetchDataFromDate])



    return (
        <div className="App">
            <span className='app-title'>Mét & Hauts</span>
            <MaterialUISwitch checked={checked} onChange={handleChange}></MaterialUISwitch>
            <Datepicker setCurrentDate={setCurrentDate} maxDate={maxDate} minDate={minDate}></Datepicker>
            <Weather vent={vent} temperature={temperature} precipitation={precipitation} cloudCover={cloudCover} max={max} min={min}></Weather>
            <Clothes sexe={checked} temperature={max} precipitation={precipitation} cloudCover={cloudCover}></Clothes>
        </div>
    );
}

export default App;
