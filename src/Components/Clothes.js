import React, { useCallback, useEffect, useState } from 'react'

const Clothes = ({ sexe, cloudCover, temperature, precipitation }) => {

    const [accessoire, setAccessoire] = useState('')
    const [tete, setTete] = useState('')
    const [haut, setHaut] = useState('')
    const [bas, setBas] = useState('')
    const [chaussures, setChaussures] = useState('')

    const tab_img = {
        'Gants': 'https://img.freepik.com/vecteurs-premium/illustration-couleur-dessin-anime-vecteur-gants-bleus-pour-ski_574806-530.jpg?w=2000',
        'Lunette_de_soleil': 'https://cdn5.coloritou.com/dessins/peindre/201810/lunettes-de-soleil-mode-101141.jpg',
        'Parapluie': 'https://cdn5.coloritou.com/dessins/peindre/202019/un-parapluie-mode-116997.jpg',
        'Jupe': 'https://static.vecteezy.com/ti/vecteur-libre/p3/17407326-vetements-jupe-mode-dessin-anime-illustrationle-vectoriel.jpg',
        'Pantalon': 'https://media.istockphoto.com/id/1076492576/fr/vectoriel/pantalon-cartoon.jpg?s=1024x1024&w=is&k=20&c=A2tUCg3SIvgedmXI0-1XUPu1vrzR7YAe_PTEOwd5NTk=',
        'Short': 'https://us.123rf.com/450wm/rubynurbaidi/rubynurbaidi1812/rubynurbaidi181200006/117626939-short-dessin-anim%C3%A9.jpg',
        'Baskets': 'https://cdn5.coloritou.com/dessins/peindre/202036/baskets-denfant-mode-119280.jpg',
        'Bottes': 'https://img.freepik.com/vecteurs-premium/bottes-caoutchouc-vert-pour-illustration-dessin-anime-vecteur-vetements-travail-jardinage_543641-201.jpg?w=2000',
        'Tongs': 'https://media.istockphoto.com/id/695354430/fr/vectoriel/ic%C3%B4ne-de-chaussons-orange-plage-isol%C3%A9-sur-fond-blanc.jpg?s=612x612&w=0&k=20&c=2L8iHMVk4hNXD-F1ByUik3dcPSyfGLb7entqcL7eZLc=',
        'Manteau': 'https://media.istockphoto.com/id/1175517903/fr/vectoriel/veste-vectorale-de-parka-rouge-de-dessin-anim%C3%A9.jpg?s=612x612&w=0&k=20&c=zbyDMWfLEwrPNcOhkzW9g4wfLaF_2yuPsgeys31_Bdc=',
        'Sweat': 'https://us.123rf.com/450wm/nikiteev/nikiteev1810/nikiteev181000146/109760984-illustration-de-dessin-anim%C3%A9-de-vecteur-sweat-%C3%A0-capuche-marron.jpg?ver=6',
        'T-shirt': 'https://static.vecteezy.com/ti/vecteur-libre/p1/7008223-cartoon-blue-casual-top-t-shirt-vectoriel.jpg',
        'Bonnet': 'https://img.freepik.com/vecteurs-premium/bonnet-hiver-chaud-couvre-chef-hiver-pompon-protection-contre-dessin-anime-ligne-doodle-froid_253359-2680.jpg?w=2000',
        'Casquette': 'https://us.123rf.com/450wm/jemastock/jemastock1705/jemastock170505072/77816467-illustration-vectorielle-de-couleur-silhouette-dessin-anim%C3%A9-sport-casquette-chapeaux.jpg',
        'None': 'https://t3.ftcdn.net/jpg/02/15/15/46/360_F_215154625_hJg9QkfWH9Cu6LCTUc8TiuV6jQSI0C5X.jpg'
    }

    const displayClothes = useCallback(() => {
        const chaud = temperature >= 25;
        const froid = temperature <= 5;
        const doux = temperature < 25 && temperature >= 15;
        const frais = temperature < 15 && temperature > 5

        const pluie = precipitation > 0 && temperature > 0;
        const neige = precipitation > 0 && temperature <= 0;

        const soleil = cloudCover < 50

        // Tete
        if (froid) {
            setTete('Bonnet')
        } else if (soleil && chaud) {
            setTete('Casquette')
        } else {
            setTete('None')
        }

        // Haut
        if (chaud || doux) {
            setHaut('T-shirt')
        } else if (frais) {
            setHaut('Sweat')
        } else if (froid) {
            setHaut('Manteau')
        } else {
            setHaut('None')
        }

        // Bas
        if (froid || frais || doux) {
            setBas('Pantalon')
        } else if (chaud && sexe) {
            setBas('Short')
        } else if (chaud && !sexe) {
            setBas('Jupe')
        } else {
            setBas('None')
        }

        // Chaussures
        if (pluie || neige) {
            setChaussures('Bottes')
        } else if (soleil && chaud) {
            setChaussures('Tongs')
        } else {
            setChaussures('Baskets')
        }

        // Accessoires
        if (pluie) {
            setAccessoire('Parapluie')
        } else if (neige) {
            setAccessoire('Gants')
        } else if (chaud && soleil) {
            setAccessoire('Lunette_de_soleil')
        } else {
            setAccessoire('None')
        }
    }, [temperature, cloudCover, precipitation, sexe])

    useEffect(() => {
        displayClothes()
    }, [sexe, cloudCover, temperature, precipitation, displayClothes])

    return (
        <div className='clothes'>
            <div className='clothes-body'>
                <div className='clothe clothe-tete'>
                    <img src={tab_img[tete]} alt='tete'></img>
                    <span>Tête</span>
                </div>
                <div className='clothe clothe-haut'>
                    <img src={tab_img[haut]} alt='haut'></img>
                    <span>Haut</span>
                </div>
                <div className='clothe clothe-bas'>
                    <img src={tab_img[bas]} alt='bas'></img>
                    <span>Bas</span>
                </div>
                <div className='clothe clothe-chaussures'>
                    <img src={tab_img[chaussures]} alt='chaussures'></img>
                    <span>Chaussures</span>
                </div>
            </div>
            <div className='clothes-accessoire'>
                <div className='clothe clothe-accessoire'>
                    <img src={tab_img[accessoire]} alt='accessoire'></img>
                    <span>Accessoire</span>
                </div>
            </div>
        </div>
    )
}

export default Clothes