import React, { useEffect, useState } from 'react'

const Weather = ({ vent, cloudCover, temperature, precipitation, max, min }) => {

    const [meteo, setMeteo] = useState(null)

    useEffect(() => {
        if (precipitation > 0) {
            if (precipitation > 0 && temperature <= 0) {
                setMeteo('neige')
            } else {
                setMeteo('pluie')
            }
        } else {
            if (cloudCover < 50){
                setMeteo('soleil')
            } else if (cloudCover > 75){
                setMeteo('nuage')
            } else {
                setMeteo('soleil_nuage')
            }
        }

    }, [vent, cloudCover, temperature, precipitation, max, min])


    return (
        <div className='weather'>
            <div className='weather-info'>
                <div className='weather-info-element weather-temperature'>
                    <p>{temperature}°C</p>
                    <span>Température moyenne</span>
                </div>
                <div className='weather-info-element weather-weather'>
                    <img src={'./images/' + meteo + '.png'} alt='weather'></img>
                    <span>Météo</span>
                </div>
                <div className='weather-info-element weather-wind'>
                    <img src='/images/vent.png' alt='wind'></img>
                    <p>{vent} km/h</p>
                    <span>Vent moyen</span>
                </div>
                <div className='weather-info-element weather-min-max'>
                    <div className='weather-min'>
                        <img src='./images/min.png' alt='min'></img>
                        <p>{min}°C</p>
                    </div>
                    <div className='weather-max'>
                        <img src='./images/max.png' alt='max'></img>
                        <p>{max}°C</p>
                    </div>
                    <span>Min / Max</span>
                </div>
            </div>
        </div>
    )
}

export default Weather