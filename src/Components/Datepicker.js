import React, { useState } from 'react';
import moment from 'moment';

const Datepicker = ({ setCurrentDate, maxDate, minDate }) => {
    const [selectedDate, setSelectedDate] = useState(moment());
    const [visibleDates, setVisibleDates] = useState(getVisibleDates(moment()));
    const [currentDay, setCurrentDay] = useState(moment().date())
    const [currentMonth, setCurrentMonth] = useState(moment().format("MMMM"))
    const [currentYear, setCurrentYear] = useState(moment().year())

    // Fonction pour obtenir les 7 prochains jours à afficher
    function getVisibleDates(date) {
        const dates = [];
        const currentDate = date;
        for (let i = 0; i < 7; i++) {
            dates.push(currentDate.clone().add(i, 'days'));
        }
        return dates;
    }

    // Fonction pour afficher les jours précédents
    function showPreviousDays() {
        const newVisibleDates = visibleDates.map(date => date.clone().subtract(7, 'days'));
        setVisibleDates(newVisibleDates);
    }

    // Fonction pour afficher les jours suivants
    function showNextDays() {
        const newVisibleDates = visibleDates.map(date => date.clone().add(7, 'days'));
        setVisibleDates(newVisibleDates);
    }

    const handleDate = (date) => {
        const newDate = moment(date)

        const min = moment(minDate)
        const max = moment(maxDate)

        if (newDate.isBetween(min, max, null, '[]')) {
            const tabDate = getVisibleDates(moment(newDate).subtract(3, 'days'));
            setVisibleDates(tabDate);
            setCurrentDate(date);
            setSelectedDate(date);
            setCurrentDay(newDate.date());
            setCurrentMonth(newDate.format('MMMM'));
            setCurrentYear(newDate.year());
        }
    }


    return (
        <div className='datepicker'>
            <div className='datepicker-warning'>
                <span className="material-symbols-outlined">
                    warning
                </span>
                <p>Attention : Le jeu de données comprend des données entre le 08 mars 2023 et le 23 juin 2023</p>
                <span className="material-symbols-outlined">
                    warning
                </span>
            </div>
            <div className='datepicker-month-year'>
                <span>{currentDay} {currentMonth} {currentYear}</span>
            </div>
            <div className='datepicker-inline-calendar'>
                <button onClick={showPreviousDays}>{'<'}</button>
                {visibleDates && visibleDates.map(date => (
                    <button key={date.format('YYYY-MM-DD')} onClick={() => handleDate(moment(date).format('YYYY-MM-DD'))} className={moment(date).isSame(moment(selectedDate)) ? 'date-selected' : null}>
                        {date.format('DD')}
                    </button>
                ))}
                <button onClick={showNextDays}>{'>'}</button>
            </div>
        </div>
    );
};

export default Datepicker;